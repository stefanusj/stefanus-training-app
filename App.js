import React, {useEffect} from 'react';
import {StatusBar, ToastAndroid} from 'react-native';
import codePush from 'react-native-code-push';
import OneSignal from 'react-native-onesignal';
import {firebase} from '@react-native-firebase/auth';
import _ from 'lodash';
import {Provider} from './src/context';
import AppNavigation from './src/navigation/routes';

const firebaseConfig = {
  apiKey: 'AIzaSyBaVvpdIW2_9kAuY1X5_rflBFhZ-cFmIMk',
  authDomain: 'stefanustrainingapp.firebaseapp.com',
  projectId: 'stefanustrainingapp',
  storageBucket: 'stefanustrainingapp.appspot.com',
  messagingSenderId: '302493114200',
  appId: '1:302493114200:web:659b93ddcf24d1cd908f2d',
};

// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App: () => React$Node = () => {
  const requireInputs = (...args) => {
    args.forEach((arg) => {
      if (!arg) {
        throw 'Harap isi semua input';
      }
    });
  };

  const handleResponseError = (error) => {
    try {
      const response = error.response;
      const message = _.values(response.data.errors)[0][0];
      ToastAndroid.show(message, ToastAndroid.SHORT);
    } catch (e) {
      console.log(error.response);
      ToastAndroid.show(error.toString(), ToastAndroid.SHORT);
    }
  };

  const syncStatusChangedCallback = (status) => {
    switch (status) {
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('AWAITING_USER_ACTION');
        break;
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('CHECKING_FOR_UPDATE');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('DOWNLOADING_PACKAGE');
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('INSTALLING_UPDATE');
        break;
      case codePush.SyncStatus.SYNC_IN_PROGRESS:
        console.log('SYNC_IN_PROGRESS');
        break;
      case codePush.SyncStatus.UNKNOWN_ERROR:
        console.log('UNKNOWN_ERROR');
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('UP_TO_DATE');
        break;
      case codePush.SyncStatus.UPDATE_IGNORED:
        console.log('UPDATE_IGNORED');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        alert('UPDATE_INSTALLED');
        break;
    }
  };

  useEffect(() => {
    OneSignal.setAppId('abd960b1-8965-4dd7-9060-e0f94128e6d8');
    OneSignal.setLogLevel(6, 0);

    OneSignal.setNotificationWillShowInForegroundHandler((event) => {
      console.log('OneSignal: notification will show in foreground:', event);
      let notification = event.getNotification();
      event.complete(notification);
    });

    OneSignal.setNotificationOpenedHandler((notification) => {
      console.log('OneSignal: notification opened:', notification);
    });

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      syncStatusChangedCallback,
    );

    return () => {
      OneSignal.clearHandlers();
    };
  }, []);

  return (
    <Provider value={{handleResponseError, requireInputs}}>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <AppNavigation />
    </Provider>
  );
};

export default App;
