import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {Divider, HorizontalView, SafeAreaContainer} from '../../components';
import {mapBoxAccessToken} from '../../constants';
import {spacing} from '../../style/spacing';

MapboxGL.setAccessToken(mapBoxAccessToken);

const coordinates = [
  [107.58011, -6.890066],
  [106.819449, -6.218465],
  [110.365231, -7.795766],
];

const HelpScreen = () => {
  const requestPermission = async () => {
    try {
      await MapboxGL.requestAndroidLocationPermissions();
    } catch (exception) {
      console.log(exception);
    }
  };

  useEffect(() => {
    requestPermission();
  }, []);

  return (
    <SafeAreaContainer>
      <MapboxGL.MapView style={styles.mapView}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((coordinate, index) => (
          <MapboxGL.PointAnnotation
            key={index}
            id={`Point${index}`}
            coordinate={coordinate}>
            <MapboxGL.Callout
              title={`Longitude ${coordinate[0]} \n Latitude ${coordinate[1]}`}
            />
          </MapboxGL.PointAnnotation>
        ))}
      </MapboxGL.MapView>
      <View style={styles.informationContainer}>
        <Divider height={4} />
        <HorizontalView style={styles.informationItem}>
          <MaterialIcons name="home" size={24} />
          <Text style={spacing.ms16}>Jakarta, Bandung, Yogyakarta</Text>
        </HorizontalView>
        <Divider height={1} />
        <HorizontalView style={styles.informationItem}>
          <MaterialIcons name="alternate-email" size={24} />
          <Text style={spacing.ms16}>customer_service@crowdfunding.com</Text>
        </HorizontalView>
        <Divider height={1} />
        <HorizontalView style={styles.informationItem}>
          <MaterialIcons name="phone" size={24} />
          <Text style={spacing.ms16}>(021) 777 - 888</Text>
        </HorizontalView>
      </View>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  mapView: {
    flex: 1,
  },
  informationContainer: {
    flex: 1,
  },
  informationItem: {
    alignItems: 'center',
    padding: 16,
  },
});

export default HelpScreen;
