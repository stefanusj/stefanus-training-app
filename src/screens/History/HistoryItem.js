import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {CurrencyFormat, HorizontalView} from '../../components';
import {types} from '../../style/types';
import {spacing} from '../../style/spacing';
import {colors} from '../../style/colors';

const HistoryItem = (history) => {
  return (
    <TouchableOpacity style={spacing.ph16}>
      <HorizontalView style={styles.container}>
        <View style={styles.menuIconContainer}>
          <MaterialIcons name="history" size={24} />
        </View>
        <View style={[spacing.m8, spacing.p8]}>
          <CurrencyFormat
            value={history.amount}
            renderText={(formattedValue) => (
              <Text style={types.subtitle2}>Total {formattedValue}</Text>
            )}
          />
          <Text>{history.order_id}</Text>
          <Text style={types.normal}>{history.created_at}</Text>
        </View>
      </HorizontalView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  menuIconContainer: {
    padding: 12,
    borderRadius: 12,
    backgroundColor: colors.normal,
  },
});

export default HistoryItem;
