import React, {useContext, useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GET_HISTORY} from '../../api';
import Context from '../../context';
import {SafeAreaContainer} from '../../components';
import {AsyncStorageToken} from '../../constants';
import HistoryItem from './HistoryItem';

const HistoryScreen = () => {
  const ctx = useContext(Context);
  const [histories, setHistories] = useState([]);

  const fetchHistories = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.get(GET_HISTORY, {
        headers: {
          Authorization: `Bearer${token}`,
        },
      });
      const {riwayat_transaksi} = data.data;
      setHistories(riwayat_transaksi);
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  useEffect(() => {
    fetchHistories();
  }, []);

  return (
    <SafeAreaContainer>
      <FlatList
        data={histories}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => HistoryItem(item)}
      />
    </SafeAreaContainer>
  );
};

export default HistoryScreen;
