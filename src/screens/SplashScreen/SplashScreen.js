import React, {useEffect, useRef} from 'react';
import {
  Animated,
  Dimensions,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {colors} from '../../style/colors';
import {types} from '../../style/types';

const height = Dimensions.get('window').height;

const SplashScreen = () => {
  const fadeOut = useRef(new Animated.Value(1)).current;
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeOut, {
      toValue: 0,
      duration: 3000,
      useNativeDriver: false,
    }).start();
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeOut, fadeIn]);

  const transformY = fadeIn.interpolate({
    inputRange: [0, 1],
    outputRange: [height, -height / 2],
  });

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.primaryColor}
          barStyle="light-content"
        />
        <Animated.View style={[styles.quotesContainer, {opacity: fadeOut}]}>
          <Text style={styles.quotes}>"Quote Example"</Text>
        </Animated.View>
        <Animated.View
          style={[
            styles.logo,
            {opacity: fadeIn, transform: [{translateY: transformY}]},
          ]}>
          <Text style={[types.h5, styles.textLogo]}>CrowdFunding</Text>
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primaryColor,
  },
  quotesContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  quotes: {
    fontSize: 14,
    color: colors.onPrimaryColor,
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textLogo: {
    color: colors.onPrimaryColor,
  },
});

export default SplashScreen;
