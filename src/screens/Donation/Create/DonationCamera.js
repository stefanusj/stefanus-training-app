import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {ButtonIcon, Container} from '../../../components';

const DonationCamera = ({camera, onTakePicture}) => {
  const [type, setType] = useState(RNCamera.Constants.Type.back);

  const onTypeClicked = () => {
    setType(
      type === RNCamera.Constants.Type.back
        ? RNCamera.Constants.Type.front
        : RNCamera.Constants.Type.back,
    );
  };

  return (
    <RNCamera style={styles.cameraContainer} type={type} ref={camera}>
      <Container style={styles.container}>
        <View style={styles.buttonCameraContainer}>
          <View style={styles.buttonCamera} />
          <View style={styles.buttonCamera}>
            <ButtonIcon
              icon="camera"
              size={72}
              iconSize={36}
              type="light"
              onPress={() => onTakePicture()}
            />
          </View>
          <View style={styles.buttonCamera}>
            <ButtonIcon
              icon={
                type === RNCamera.Constants.Type.back
                  ? 'camera-front'
                  : 'camera-rear'
              }
              size={48}
              iconSize={24}
              type="light"
              onPress={() => onTypeClicked()}
            />
          </View>
        </View>
      </Container>
    </RNCamera>
  );
};

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
  },
  container: {
    justifyContent: 'flex-end',
    alignItems: 'stretch',
  },
  buttonCamera: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 64,
  },
  buttonCameraContainer: {
    justifyContent: 'center',
    alignItems: 'stretch',
    flexDirection: 'row',
  },
});

export default DonationCamera;
