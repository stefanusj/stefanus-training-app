import React, {useContext, useRef, useState} from 'react';
import {
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {STORE_DONATION} from '../../../api';
import {
  Button,
  Label,
  SafeAreaContainer,
  TextInput,
  TextInputMask,
} from '../../../components';
import Context from '../../../context';
import {AsyncStorageToken} from '../../../constants';
import {spacing} from '../../../style/spacing';
import {colors} from '../../../style/colors';
import DonationCamera from './DonationCamera';

const DonationCreateScreen = ({navigation}) => {
  const ctx = useContext(Context);
  let camera = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [screen, setScreen] = useState({});

  const onImageClicked = () => {
    setIsVisible(true);
  };

  const onStoreClicked = async () => {
    try {
      ctx.requireInputs(
        screen.title,
        screen.description,
        screen.donation,
        screen.picture,
      );
      const formData = new FormData();
      formData.append('title', screen.title);
      formData.append('description', screen.description);
      formData.append('donation', screen.donation);
      formData.append('photo', {
        uri: screen.picture,
        name: 'photo.jpg',
        type: 'image/jpg',
      });

      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.post(STORE_DONATION, formData, {
        headers: {
          Authorization: `Bearer${token}`,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      });

      ToastAndroid.show(data.response_message.toString(), ToastAndroid.SHORT);
      navigation.goBack();
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onTakePicture = async () => {
    try {
      const options = {quality: 0.5, base64: true};
      if (camera) {
        const picture = await camera.current.takePictureAsync(options);
        setScreen({...screen, picture: picture.uri});
        setIsVisible(false);
      }
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  return (
    <SafeAreaContainer>
      <ScrollView>
        <TouchableOpacity onPress={() => onImageClicked()}>
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{uri: screen.picture}} />
            <View style={styles.imageLabel}>
              <MaterialIcons name="camera-alt" size={24} />
              <Text>Pilih Gambar</Text>
            </View>
          </View>
        </TouchableOpacity>
        <View style={spacing.ph16}>
          <Label style={spacing.mt16}>Judul</Label>
          <TextInput
            placeholder="Judul"
            value={screen.title}
            onChangeText={(title) => setScreen({...screen, title})}
          />
          <Label style={spacing.mt16}>Deskripsi</Label>
          <TextInput
            placeholder="Deskripsi"
            multiline={true}
            numberOfLines={5}
            value={screen.description}
            onChangeText={(description) => setScreen({...screen, description})}
          />
          <Label style={spacing.mt16}>Dana yang dibutuhkan</Label>
          <TextInputMask
            type="money"
            options={{
              precision: 0,
              unit: 'Rp',
            }}
            value={screen.donation}
            placeholder="Rp0"
            placeholderTextColor={colors.BLACK}
            includeRawValueInChangeText={true}
            onChangeText={(maskedDonation, donation) =>
              setScreen({...screen, donation})
            }
          />
          <Button
            style={spacing.mv16}
            text="Buat"
            type="solid"
            onPress={() => onStoreClicked()}
          />
          <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
            <DonationCamera
              camera={camera}
              onTakePicture={() => onTakePicture()}
            />
          </Modal>
        </View>
      </ScrollView>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    margin: 16,
    borderRadius: 32,
  },
  image: {
    height: 256,
    width: '100%',
    backgroundColor: colors.divider,
    borderRadius: 32,
  },
  imageLabel: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});

export default DonationCreateScreen;
