import React, {useContext, useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GET_PROFILE, PAY_DONATION} from '../../../api';
import Context from '../../../context';
import {
  Button,
  Label,
  SafeAreaContainer,
  TextInputMask,
} from '../../../components';
import {colors} from '../../../style/colors';
import {AsyncStorageToken} from '../../../constants';
import {spacing} from '../../../style/spacing';
import DonateItem from './DonateItem';

const donates = [
  {
    id: 1,
    amount: 10000,
  },
  {
    id: 2,
    amount: 20000,
  },
  {
    id: 3,
    amount: 50000,
  },
  {
    id: 4,
    amount: 100000,
  },
];

const DonationDonateScreen = ({navigation}) => {
  const ctx = useContext(Context);
  const [screen, setScreen] = useState({
    donate: {
      amount: 0,
    },
    profile: {},
  });

  const fetchProfile = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.get(GET_PROFILE, {
        headers: {
          Authorization: `Bearer${token}`,
        },
      });
      const {profile} = data.data;
      setScreen({...screen, profile});
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onDonateItemClicked = (donate) => {
    setScreen({...screen, donate});
  };

  const onNextClicked = async () => {
    try {
      if (screen.donate.amount === 0) {
        throw 'Minimal donasi adalah Rp1';
      }
      const time = new Date().getTime();
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.post(
        PAY_DONATION,
        {
          transaction_details: {
            order_id: `Donasi-${time}`,
            gross_amount: screen.donate.amount,
            donation_id: screen.donate?.id ?? 0,
          },
          customer_details: {
            first_name: screen.profile.name,
            email: screen.profile.email,
          },
        },
        {
          headers: {
            Authorization: `Bearer${token}`,
          },
        },
      );
      const {midtrans} = data.data;
      navigation.replace('Payment', {midtrans});
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  useEffect(() => {
    fetchProfile();
  }, []);

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <FlatList
        data={donates}
        extraData={screen}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) =>
          DonateItem(item, screen.donate.amount, onDonateItemClicked)
        }
      />
      <Label style={spacing.mt32}>Jumlah donasi</Label>
      <TextInputMask
        type={'money'}
        options={{
          precision: 0,
          unit: 'Rp',
        }}
        value={screen.donate.amount.toString()}
        placeholder="Rp0"
        placeholderTextColor={colors.BLACK}
        includeRawValueInChangeText={true}
        onChangeText={(maskedAmount, amount) =>
          setScreen({...screen, donate: {amount}})
        }
      />
      <Button
        text="Lanjut"
        type="solid"
        style={spacing.mv16}
        onPress={() => onNextClicked()}
      />
    </SafeAreaContainer>
  );
};

export default DonationDonateScreen;
