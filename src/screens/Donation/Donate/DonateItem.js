import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {types} from '../../../style/types';
import {CardView, CurrencyFormat} from '../../../components';
import {colors} from '../../../style/colors';

const DonateItem = (donation, amount, onClick) => {
  return (
    <TouchableOpacity onPress={() => onClick(donation)}>
      <CardView
        style={[
          styles.container,
          amount === donation.amount ? {backgroundColor: colors.BLACK} : '',
        ]}>
        <CurrencyFormat
          value={donation.amount}
          renderText={(formattedValue) => (
            <Text
              style={[
                types.subtitle1,
                amount === donation.amount
                  ? {color: colors.WHITE}
                  : {color: colors.BLACK},
              ]}>
              {formattedValue}
            </Text>
          )}
        />
        <MaterialIcons
          name="chevron-right"
          size={24}
          color={amount === donation.amount ? colors.WHITE : colors.BLACK}
        />
      </CardView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
    marginVertical: 8,
  },
});

export default DonateItem;
