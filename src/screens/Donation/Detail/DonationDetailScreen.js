import React, {useEffect, useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {imageUri} from '../../../api';
import {
  Button,
  CardView,
  HorizontalView,
  SafeAreaContainer,
} from '../../../components';
import {spacing} from '../../../style/spacing';
import {types} from '../../../style/types';

const DonationDetailScreen = ({navigation, route}) => {
  const [donation, setDonation] = useState({});

  const onDonateClicked = () => {
    navigation.navigate('DonationDonate');
  };

  useEffect(() => {
    setDonation(route.params?.donation);
  }, []);

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Image style={styles.image} source={{uri: imageUri(donation?.photo)}} />
        <Text style={[types.h5, spacing.mv16]}>{donation?.title}</Text>
        <Text style={types.normal}>
          <MaterialIcons name="alarm" /> {donation?.created_at}
        </Text>
        <Text style={[types.subtitle1, spacing.mt16]}>Deskripsi</Text>
        <Text style={spacing.mt8}>{donation.description}</Text>
        <Text style={[types.subtitle1, spacing.mt16]}>
          Informasi Penggalang Dana
        </Text>
        <CardView style={styles.profile}>
          <HorizontalView style={styles.profileContainer}>
            <Image
              style={styles.avatar}
              source={{uri: imageUri(donation?.user?.photo)}}
            />
            <View style={spacing.ms16}>
              <Text style={types.subtitle1}>
                {donation?.user?.name}
                <MaterialIcons name="verified-user" />
              </Text>
              <Text>Identitas Terverifikasi</Text>
            </View>
          </HorizontalView>
        </CardView>
        <Button
          text="Donasi"
          type="solid"
          style={spacing.mv16}
          onPress={() => onDonateClicked()}
        />
      </ScrollView>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 256,
    borderRadius: 16,
  },
  profile: {
    padding: 16,
    marginTop: 16,
  },
  profileContainer: {
    alignItems: 'center',
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 36,
  },
});

export default DonationDetailScreen;
