import React, {useContext, useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GET_DONATION} from '../../api';
import {SafeAreaContainer} from '../../components';
import {AsyncStorageToken} from '../../constants';
import Context from '../../context';
import DonationItem from './DonationItem';

const DonationScreen = ({navigation}) => {
  const ctx = useContext(Context);
  const [donations, setDonations] = useState({});

  const fetchDonation = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.get(GET_DONATION, {
        headers: {
          Authorization: `Bearer${token}`,
        },
      });
      const {donasi} = data.data;
      setDonations(donasi);
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onDonationClicked = (donation) => {
    navigation.navigate('DonationDetail', {donation});
  };

  useEffect(() => {
    fetchDonation();
  }, []);

  return (
    <SafeAreaContainer>
      <FlatList
        data={donations}
        extraData={donations}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => DonationItem(item, onDonationClicked)}
      />
    </SafeAreaContainer>
  );
};

export default DonationScreen;
