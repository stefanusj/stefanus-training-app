import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {types} from '../../style/types';
import {CardView, CurrencyFormat} from '../../components';
import {imageUri} from '../../api';
import {spacing} from '../../style/spacing';

const DonationItem = (donation, onClick) => {
  return (
    <TouchableOpacity onPress={() => onClick(donation)}>
      <CardView style={styles.container}>
        <Image source={{uri: imageUri(donation.photo)}} style={styles.image} />
        <View style={spacing.ms16}>
          <Text style={types.subtitle2}>{donation.title}</Text>
          <Text>{donation.user?.name}</Text>
          <CurrencyFormat
            value={donation.donation}
            renderText={(formattedValue) => (
              <Text>Total donasi: {formattedValue}</Text>
            )}
          />
        </View>
      </CardView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 8,
    margin: 8,
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 16,
  },
});

export default DonationItem;
