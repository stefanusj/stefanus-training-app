import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {spacing} from '../../style/spacing';
import {types} from '../../style/types';

const IntroItem = (intro) => {
  return (
    <View style={styles.container}>
      <Image source={intro.image} style={styles.image} resizeMode="contain" />
      <Text style={[types.h6, spacing.mt16]}>{intro.title}</Text>
      <Text style={[types.body2, styles.description]}>{intro.description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 256,
    width: 256,
  },
  description: {
    margin: 16,
    textAlign: 'center',
  },
});

export default IntroItem;
