import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button, SafeAreaContainer, Slider} from '../../components';
import {spacing} from '../../style/spacing';
import {types} from '../../style/types';
import IntroItem from './IntroItem';

const data = [
  {
    id: 1,
    image: require('../../assets/images/intro_mobile.png'),
    title: 'Mobile Integration',
    description: 'Terintegrasi dengan aplikasi mobile anda.',
  },
  {
    id: 2,
    image: require('../../assets/images/intro_safe.png'),
    title: 'Safe',
    description: 'Donasi anda akan aman dan langsung ditujukan ke orang yang tepat.',
  },
  {
    id: 3,
    image: require('../../assets/images/intro_gift.png'),
    title: 'Profit',
    description: 'Lengkapi hidup dengan saling berbagi.',
  },
];

const IntroScreen = ({navigation}) => {
  const onLoginClicked = () => {
    navigation.navigate('Login');
  };

  const onRegisterClicked = () => {
    navigation.navigate('Register');
  };

  return (
    <SafeAreaContainer>
      <Text style={[types.h5, styles.textLogo]}>CrowdFunding</Text>
      <Slider
        style={styles.slider}
        data={data}
        extraData={data}
        renderItem={({item}) => IntroItem(item)}
        keyExtractor={(item) => item.id.toString()}
      />
      <View style={spacing.p16}>
        <Button
          style={spacing.mv8}
          text="Masuk"
          type="solid"
          onPress={() => onLoginClicked()}
        />
        <Button
          style={spacing.mv8}
          text="Daftar"
          type="outline"
          onPress={() => onRegisterClicked()}
        />
      </View>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  textLogo: {
    padding: 32,
    textAlign: 'center',
  },
  slider: {
    flex: 1,
  },
});

export default IntroScreen;
