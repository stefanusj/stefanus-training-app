import React, {useState} from 'react';
import {processColor, StyleSheet} from 'react-native';
import BarChart from 'react-native-charts-wrapper/lib/BarChart';
import {SafeAreaContainer} from '../../components';
import {colors} from '../../style/colors';

const data = [
  {y: 100},
  {y: 60},
  {y: 90},
  {y: 45},
  {y: 67},
  {y: 32},
  {y: 150},
  {y: 70},
  {y: 40},
  {y: 89},
];

const StatisticsScreen = () => {
  const [chart] = useState({
    data: {
      dataSets: [
        {
          values: data,
          label: '',
          config: {
            colors: [processColor(colors.primaryColor)],
            drawValues: false,
          },
        },
      ],
    },
  });

  const [legend] = useState({
    enabled: false,
  });

  const [xAxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'Mei',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Oct',
      'Nov',
      'Des',
    ],
    position: 'BOTTOM',
    labelRotationAngle: -45,
    drawGridLines: false,
  });

  const [yAxis] = useState({
    left: {
      axisMinimum: 0,
      drawGridLines: false,
      granularity: 30,
    },
    right: {
      enabled: false,
    },
  });

  return (
    <SafeAreaContainer>
      <BarChart
        chartDescription={{text: ''}}
        data={chart.data}
        legend={legend}
        xAxis={xAxis}
        yAxis={yAxis}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        style={styles.chart}
      />
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  chart: {
    flex: 1,
  },
});

export default StatisticsScreen;
