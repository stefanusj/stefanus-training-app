import React, {useContext, useEffect, useState} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GET_PROFILE, imageUri} from '../../api';
import {SafeAreaContainer} from '../../components';
import {AsyncStorageToken} from '../../constants';
import Context from '../../context';

const InboxScreen = () => {
  const ctx = useContext(Context);
  const [messages, setMessages] = useState([]);
  const [profile, setProfile] = useState({});

  const fetchProfile = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.get(GET_PROFILE, {
        headers: {
          Authorization: `Bearer${token}`,
        },
      });
      const {profile} = data.data;
      setProfile(profile);
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onChildAddedListener = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((prevMessage) => GiftedChat.append(prevMessage, value));
      });
  };

  const onMessageSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  useEffect(() => {
    fetchProfile();
    onChildAddedListener();

    return () => {
      database().ref('messages').off();
    };
  }, []);

  return (
    <SafeAreaContainer>
      <GiftedChat
        messages={messages}
        showUserAvatar={true}
        user={{
          _id: profile?.id,
          name: profile?.name,
          avatar: imageUri(profile?.photo),
        }}
        onSend={(message) => onMessageSend(message)}
      />
    </SafeAreaContainer>
  );
};

export default InboxScreen;
