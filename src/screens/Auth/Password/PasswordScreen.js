import React, {useContext, useState} from 'react';
import {Text, ToastAndroid} from 'react-native';
import Axios from 'axios';
import {UPDATE_PASSWORD} from '../../../api';
import {
  Button,
  Label,
  SafeAreaContainer,
  TextInputEndIcon,
} from '../../../components';
import Context from '../../../context';
import {spacing} from '../../../style/spacing';
import {types} from '../../../style/types';

const PasswordScreen = ({navigation, route}) => {
  const ctx = useContext(Context);
  const [passwordSecured, setPasswordSecured] = useState(true);
  const [
    passwordConfirmationSecured,
    setPasswordConfirmationSecured,
  ] = useState(true);
  const [screen, setScreen] = useState({
    password: '',
    passwordConfirmation: '',
  });

  const onIconPasswordClicked = () => {
    setPasswordSecured(!passwordSecured);
  };

  const onIconPasswordSecuredClicked = () => {
    setPasswordConfirmationSecured(!passwordConfirmationSecured);
  };

  const onPasswordSetClicked = async () => {
    try {
      ctx.requireInputs(screen.password, screen.passwordConfirmation);
      if (screen.password !== screen.passwordConfirmation) {
        throw 'Konfirmasi password tidak sama';
      }

      const {data} = await Axios.post(UPDATE_PASSWORD, {
        email: route.params?.email,
        password: screen.password,
        password_confirmation: screen.passwordConfirmation,
      });

      ToastAndroid.show(data.response_message.toString(), ToastAndroid.SHORT);
      navigation.popToTop();
      navigation.navigate('Login');
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <Text style={types.h4}>Password</Text>
      <Text style={[types.body1, spacing.mt16]}>
        Amankan akun anda dengan kata sandi.
      </Text>
      <Label style={spacing.mt32}>Password</Label>
      <TextInputEndIcon
        icon={passwordSecured ? 'visibility' : 'visibility-off'}
        secureTextEntry={passwordSecured}
        value={screen.password}
        onChangeText={(password) => setScreen({...screen, password})}
        onIconCLicked={() => onIconPasswordClicked()}
      />
      <Label style={spacing.mt16}>Konfirmasi Password</Label>
      <TextInputEndIcon
        icon={passwordConfirmationSecured ? 'visibility' : 'visibility-off'}
        secureTextEntry={passwordConfirmationSecured}
        value={screen.passwordConfirmation}
        onChangeText={(passwordConfirmation) =>
          setScreen({...screen, passwordConfirmation})
        }
        onIconCLicked={() => onIconPasswordSecuredClicked()}
      />
      <Button
        style={spacing.mt32}
        text="Daftar"
        type="solid"
        onPress={() => onPasswordSetClicked()}
      />
    </SafeAreaContainer>
  );
};

export default PasswordScreen;
