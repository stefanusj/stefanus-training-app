import React, {useContext, useState} from 'react';
import {Text, ToastAndroid} from 'react-native';
import Axios from 'axios';
import {REGISTER} from '../../../api';
import {Button, Label, SafeAreaContainer, TextInput} from '../../../components';
import Context from '../../../context';
import {spacing} from '../../../style/spacing';
import {types} from '../../../style/types';

const RegisterScreen = ({navigation}) => {
  const ctx = useContext(Context);
  const [screen, setScreen] = useState({});

  const onRegisterClicked = async () => {
    try {
      ctx.requireInputs(screen.name, screen.email);
      const {data} = await Axios.post(REGISTER, {
        name: screen.name,
        email: screen.email,
      });

      ToastAndroid.show(data.response_message.toString(), ToastAndroid.SHORT);
      const {user} = data.data;
      navigation.navigate('Verification', {
        name: user.name,
        email: user.email,
      });
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <Text style={types.h4}>Register</Text>
      <Text style={[types.body1, spacing.mt16]}>
        Daftarkan diri anda dan mulai berbagi hari ini.
      </Text>
      <Label style={spacing.mt32}>Name</Label>
      <TextInput
        placeholder="Eg. John Doe"
        value={screen.name}
        onChangeText={(name) => setScreen({...screen, name})}
      />
      <Label style={spacing.mt16}>Email</Label>
      <TextInput
        placeholder="example@mail.com"
        value={screen.email}
        onChangeText={(email) => setScreen({...screen, email})}
      />
      <Button
        style={spacing.mt32}
        text="Daftar"
        type="solid"
        onPress={() => onRegisterClicked()}
      />
    </SafeAreaContainer>
  );
};

export default RegisterScreen;
