import React, {useContext, useState} from 'react';
import {Text, ToastAndroid} from 'react-native';
import Axios from 'axios';
import {REGENERATE_OTP, VERIFICATION} from '../../../api';
import {Button, OTPInputView, SafeAreaContainer} from '../../../components';
import Context from '../../../context';
import {spacing} from '../../../style/spacing';
import {types} from '../../../style/types';

const VerificationScreen = ({navigation, route}) => {
  const ctx = useContext(Context);
  const [screen, setScreen] = useState({});

  const onResendCodeClicked = async () => {
    try {
      const {data} = await Axios.post(REGENERATE_OTP, {
        email: route.params?.email,
      });

      ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onVerificationClicked = async () => {
    try {
      ctx.requireInputs(screen.otp);
      const {data} = await Axios.post(VERIFICATION, {
        otp: screen.otp,
      });
      if (data.response_code !== '00') {
        throw data.response_message;
      }

      ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
      navigation.navigate('Password', {...route.params});
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <Text style={types.h4}>Verification</Text>
      <Text style={[types.body1, spacing.mt16]}>
        Masukan konfirmasi kode yang kami kirimkan ke {route.params?.email}.
      </Text>
      <OTPInputView pinCount={6} onCodeFilled={(otp) => setScreen({otp})} />
      <Button
        text="Resend Code"
        type="outline"
        onPress={() => onResendCodeClicked()}
      />
      <Button
        style={spacing.mt16}
        text="Verifikasi"
        type="solid"
        onPress={() => onVerificationClicked()}
      />
    </SafeAreaContainer>
  );
};

export default VerificationScreen;
