export const LoginPlatform = Object.freeze({
  GOOGLE: 'GOOGLE',
  FINGERPRINT: 'FINGERPRINT',
  NORMAL: 'NORMAL',
});
