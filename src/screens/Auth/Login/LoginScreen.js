import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, ToastAndroid, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GoogleSignin} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';
import {LOGIN} from '../../../api';
import {
  Button,
  Divider,
  HorizontalView,
  Label,
  SafeAreaContainer,
  TextInput,
  TextInputEndIcon,
} from '../../../components';
import {colors} from '../../../style/colors';
import {spacing} from '../../../style/spacing';
import {types} from '../../../style/types';
import {AsyncStorageToken, webClientId} from '../../../constants';
import {LoginPlatform} from './LoginPlatform';
import Context from '../../../context';

const LoginScreen = ({navigation}) => {
  const ctx = useContext(Context);
  const [passwordSecured, setPasswordSecured] = useState(true);
  const [screen, setScreen] = useState({
    email: undefined,
    password: undefined,
  });

  const storeToken = async (token) => {
    await AsyncStorage.setItem(AsyncStorageToken, token);
  };

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({webClientId});
  };

  const signInGoogle = async () => {
    const {idToken} = await GoogleSignin.signIn();
    const credential = auth.GoogleAuthProvider.credential(idToken);
    return auth().signInWithCredential(credential);
  };

  const signInFingerprint = async () => {
    return TouchID.authenticate();
  };

  const onIconPasswordClicked = () => {
    setPasswordSecured(!passwordSecured);
  };

  const onLoginClicked = async (from) => {
    try {
      switch (from) {
        case LoginPlatform.GOOGLE: {
          await signInGoogle();
          await storeToken(LoginPlatform.GOOGLE);
          break;
        }
        case LoginPlatform.FINGERPRINT: {
          await signInFingerprint();
          await storeToken(LoginPlatform.FINGERPRINT);
          break;
        }
        default: {
          ctx.requireInputs(screen.email, screen.password);
          const {data} = await Axios.post(LOGIN, {
            email: screen.email,
            password: screen.password,
          });
          await storeToken(data.data.token);
        }
      }
      navigation.reset({
        index: 0,
        routes: [{name: 'Dashboard'}],
      });
    } catch (exception) {
      if (exception.response) {
        ToastAndroid.show('Login gagal', ToastAndroid.SHORT);
      } else {
        ToastAndroid.show(exception.toString(), ToastAndroid.SHORT);
      }
    }
  };

  const onRegisterCLicked = () => {
    navigation.navigate('Register');
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <Text style={types.h4}>Login</Text>
      <Text style={[types.body1, spacing.mt16]}>
        Masuk dan bergabunglah bersama yang lainnya.
      </Text>
      <Label style={spacing.mt32}>Email</Label>
      <TextInput
        placeholder="example@mail.com"
        value={screen.email}
        onChangeText={(email) => setScreen({...screen, email})}
      />
      <Label style={spacing.mt16}>Password</Label>
      <TextInputEndIcon
        icon={passwordSecured ? 'visibility' : 'visibility-off'}
        secureTextEntry={passwordSecured}
        value={screen.password}
        onChangeText={(password) => setScreen({...screen, password})}
        onIconCLicked={() => onIconPasswordClicked()}
      />
      <Button
        style={spacing.mt32}
        text="Masuk"
        type="solid"
        onPress={() => onLoginClicked(LoginPlatform.NORMAL)}
      />
      <Divider style={spacing.mv16} height={1} />
      <HorizontalView style={styles.buttonSignInAnotherContainer}>
        <Button
          style={styles.buttonSignInAnother}
          text="Google"
          type="outline"
          onPress={() => onLoginClicked(LoginPlatform.GOOGLE)}
        />
        <Button
          style={styles.buttonSignInAnother}
          text="Fingerprint"
          type="outline"
          onPress={() => onLoginClicked(LoginPlatform.FINGERPRINT)}
        />
      </HorizontalView>
      <HorizontalView style={styles.registerContainer}>
        <Text>Belum punya akun?</Text>
        <TouchableOpacity onPress={() => onRegisterCLicked()}>
          <Text style={styles.register}>Daftar</Text>
        </TouchableOpacity>
      </HorizontalView>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  buttonSignInAnotherContainer: {
    justifyContent: 'space-between',
  },
  buttonSignInAnother: {
    flexBasis: '48%',
  },
  registerContainer: {
    justifyContent: 'center',
    padding: 16,
  },
  register: {
    marginStart: 4,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
});

export default LoginScreen;
