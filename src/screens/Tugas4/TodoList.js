import React, {useContext} from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {RootContext} from './index';

const TodoList = () => {
  const state = useContext(RootContext);

  const Todo = ({todo}) => (
    <View style={styles.todoContainer}>
      <View style={styles.todoContent}>
        <Text>{todo.date}</Text>
        <Text>{todo.title}</Text>
      </View>
      <TouchableOpacity onPress={() => state.destroyTodo(todo.title)}>
        <Ionicons name="trash-outline" size={24} />
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Masukan Todolist</Text>
      <View style={styles.horizontal}>
        <TextInput
          style={styles.textInput}
          placeholder="Input Here"
          value={state.input}
          onChangeText={(text) => state.setInput(text)}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => state.storeTodo()}>
          <Ionicons name="add-outline" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <FlatList
        data={state.todos}
        renderItem={(todo) => <Todo todo={todo.item} />}
        keyExtractor={(todo) => todo.title}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  horizontal: {
    flexDirection: 'row',
  },
  title: {
    paddingVertical: 8,
  },
  textInput: {
    flexGrow: 1,
    marginEnd: 8,
    borderColor: 'gray',
    borderWidth: 1,
    height: 48,
  },
  button: {
    backgroundColor: '#0496FF',
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
    width: 48,
  },

  todoContainer: {
    flex: 1,
    flexDirection: 'row',
    borderColor: 'lightgray',
    borderWidth: 4,
    borderRadius: 8,
    marginTop: 16,
    padding: 16,
  },
  todoContent: {
    flex: 1,
  },
});

export default TodoList;
