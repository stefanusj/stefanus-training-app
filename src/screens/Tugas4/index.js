import React, {createContext, useState} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Tugas4 = () => {
  const [todos, setTodos] = useState([]);

  const [input, setInput] = useState('');
  const storeTodo = () => {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let todoDate = `${day}/${month}/${year}`;
    setTodos([...todos, {date: todoDate, title: input}]);
    setInput('');
  };
  const destroyTodo = (title) => {
    setTodos(todos.filter((todo) => todo.title !== title));
  };
  return (
    <RootContext.Provider
      value={{todos, input, setInput, storeTodo, destroyTodo}}>
      <TodoList />
    </RootContext.Provider>
  );
};
export default Tugas4;
