import React, {useContext, useEffect, useRef, useState} from 'react';
import {Image, Modal, StyleSheet, ToastAndroid, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {GET_PROFILE, imageUri, UPDATE_PROFILE} from '../../../api';
import {
  Button,
  ButtonIcon,
  Label,
  SafeAreaContainer,
  TextInput,
  TextInputEndIcon,
} from '../../../components';
import {spacing} from '../../../style/spacing';
import {AsyncStorageToken} from '../../../constants';
import Context from '../../../context';
import AccountEditCamera from './AccountEditCamera';

const AccountEditScreen = () => {
  const ctx = useContext(Context);
  const camera = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [nameEditable, setNameEditable] = useState(false);
  const [photo, setPhoto] = useState(null);
  const [screen, setScreen] = useState({});

  const fetchProfile = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.get(GET_PROFILE, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const {profile} = data.data;
      setScreen({
        ...screen,
        avatar: imageUri(profile.photo),
        name: profile.name,
        email: profile.email,
      });
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onCameraClicked = () => {
    setIsVisible(true);
  };

  const onIconEditClicked = () => {
    setNameEditable(!nameEditable);
  };

  const onTakePicture = async () => {
    try {
      const options = {quality: 0.5, base64: true};
      if (camera) {
        const picture = await camera.current.takePictureAsync(options);
        setPhoto(picture.uri);
        setIsVisible(false);
      }
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const onUpdateClicked = async () => {
    try {
      ctx.requireInputs(screen.name);
      const formData = new FormData();
      formData.append('name', screen.name);
      if (photo) {
        formData.append('photo', {
          uri: photo,
          name: 'photo.jpg',
          type: 'image/jpg',
        });
      }

      const token = await AsyncStorage.getItem(AsyncStorageToken);
      const {data} = await Axios.post(UPDATE_PROFILE, formData, {
        headers: {
          Authorization: `Bearer${token}`,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      });

      ToastAndroid.show(data.response_message.toString(), ToastAndroid.SHORT);
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  useEffect(() => {
    fetchProfile();
  }, []);

  return (
    <SafeAreaContainer style={spacing.ph16}>
      <View style={styles.profileContainer}>
        <View>
          <Image
            style={styles.avatar}
            source={{uri: photo ? photo : screen?.avatar}}
          />
          <ButtonIcon
            icon="camera-alt"
            size={48}
            iconSize={24}
            type="solid"
            style={styles.profileCamera}
            onPress={() => onCameraClicked()}
          />
        </View>
      </View>
      <View>
        <Label style={spacing.mt32}>Nama Lengkap</Label>
        <TextInputEndIcon
          icon="edit"
          editable={nameEditable}
          value={screen.name}
          onChangeText={(name) => setScreen({...screen, name})}
          onIconCLicked={() => onIconEditClicked()}
        />
        <Label style={spacing.mt16}>Email</Label>
        <TextInput
          editable={false}
          placeholder="example@mail.com"
          value={screen.email}
          onChangeText={(email) => setScreen({...screen, email})}
        />
        <Button
          style={spacing.mt16}
          text="Simpan"
          type="solid"
          onPress={() => onUpdateClicked()}
        />
      </View>
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <AccountEditCamera
          camera={camera}
          onTakePicture={() => onTakePicture()}
        />
      </Modal>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  profileContainer: {
    padding: 16,
    alignItems: 'center',
  },
  profileCamera: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  avatar: {
    borderRadius: 144,
    width: 144,
    height: 144,
  },
});

export default AccountEditScreen;
