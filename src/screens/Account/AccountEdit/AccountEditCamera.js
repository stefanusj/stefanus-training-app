import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {ButtonIcon} from '../../../components';

const AccountEditCamera = ({camera, onTakePicture}) => {
  const [type, setType] = useState(RNCamera.Constants.Type.back);

  const onTypeClicked = () => {
    setType(
      type === RNCamera.Constants.Type.back
        ? RNCamera.Constants.Type.front
        : RNCamera.Constants.Type.back,
    );
  };

  return (
    <RNCamera style={styles.cameraContainer} type={type} ref={camera}>
      <View style={styles.container}>
        <ButtonIcon
          icon={
            type === RNCamera.Constants.Type.back
              ? 'camera-front'
              : 'camera-rear'
          }
          size={48}
          iconSize={24}
          type="light"
          style={styles.buttonSwap}
          onPress={() => onTypeClicked()}
        />
        <View style={styles.face} />
        <View style={styles.card} />
        <View style={styles.buttonCameraContainer}>
          <ButtonIcon
            icon="camera"
            size={72}
            iconSize={36}
            type="light"
            onPress={() => onTakePicture()}
          />
        </View>
      </View>
    </RNCamera>
  );
};

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  face: {
    width: 192,
    height: 192,
    marginVertical: 48,
    borderRadius: 192,
    borderWidth: 1,
  },
  card: {
    width: 192,
    height: 96,
    marginVertical: 36,
    borderWidth: 1,
  },
  buttonSwap: {
    alignSelf: 'flex-start',
    margin: 16,
  },
  buttonCameraContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default AccountEditCamera;
