import React, {useCallback, useContext, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import {useFocusEffect} from '@react-navigation/native';
import Axios from 'axios';
import {GET_PROFILE, imageUri} from '../../api';
import {Divider, HorizontalView, SafeAreaContainer} from '../../components';
import {AsyncStorageToken} from '../../constants';
import Context from '../../context';
import {colors} from '../../style/colors';
import {spacing} from '../../style/spacing';
import {types} from '../../style/types';
import {LoginPlatform} from '../Auth/Login/LoginPlatform';

const AccountScreen = ({navigation}) => {
  const ctx = useContext(Context);
  const [screen, setScreen] = useState({});

  const fetchProfileToken = async (token) => {
    const {data} = await Axios.get(GET_PROFILE, {
      headers: {
        Authorization: `Bearer${token}`,
      },
    });
    const {profile} = data.data;
    setScreen({
      avatar: imageUri(profile.photo),
      name: profile.name,
    });
  };

  const fetchProfileGoogle = () => {
    const user = auth().currentUser;
    setScreen({
      avatar: user.photoURL,
      name: user.displayName,
    });
  };

  const fetchProfileFingerprint = () => {
    setScreen({
      avatar: 'https://picsum.photos/200',
      name: 'FINGERPRINT',
    });
  };

  const fetchProfile = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      if (token === LoginPlatform.GOOGLE) {
        fetchProfileGoogle();
      } else if (token === LoginPlatform.FINGERPRINT) {
        fetchProfileFingerprint();
      } else {
        await fetchProfileToken(token);
      }
    } catch (exception) {
      ctx.handleResponseError(exception);
    }
  };

  const destroyToken = async () => {
    const token = await AsyncStorage.getItem(AsyncStorageToken);
    if (token === LoginPlatform.GOOGLE) {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } else {
    }
    return AsyncStorage.removeItem(AsyncStorageToken);
  };

  const onAccountClicked = () => {
    navigation.navigate('AccountEdit');
  };

  const onHelpClicked = () => {
    navigation.navigate('Help');
  };

  const onLogoutClicked = async () => {
    try {
      await destroyToken();
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (exception) {
      ToastAndroid.show(exception.toString(), ToastAndroid.SHORT);
    }
  };

  useFocusEffect(
    useCallback(() => {
      fetchProfile();
    }, []),
  );

  return (
    <SafeAreaContainer>
      <TouchableOpacity
        style={styles.profileContainer}
        onPress={() => onAccountClicked()}>
        <HorizontalView style={styles.profile}>
          <Image style={styles.avatar} source={{uri: screen?.avatar}} />
          <Text style={[types.h6, spacing.ms16]}>{screen.name}</Text>
        </HorizontalView>
      </TouchableOpacity>
      <View style={styles.settingContainer}>
        <TouchableOpacity>
          <HorizontalView style={styles.settingsItem}>
            <MaterialIcons
              name="account-balance-wallet"
              size={24}
              color="black"
            />
            <Text style={styles.settingsItemTitle}>Saldo</Text>
            <Text style={styles.settingsItemText}>Rp120.000.000</Text>
          </HorizontalView>
        </TouchableOpacity>
        <Divider height={1} />
        <TouchableOpacity>
          <HorizontalView style={styles.settingsItem}>
            <MaterialIcons name="settings" size={24} color="black" />
            <Text style={styles.settingsItemTitle}>Pengaturan</Text>
          </HorizontalView>
        </TouchableOpacity>
        <Divider height={1} />
        <TouchableOpacity onPress={() => onHelpClicked()}>
          <HorizontalView style={styles.settingsItem}>
            <MaterialIcons name="help-outline" size={24} color="black" />
            <Text style={styles.settingsItemTitle}>Bantuan</Text>
          </HorizontalView>
        </TouchableOpacity>
        <Divider height={1} />
        <TouchableOpacity>
          <HorizontalView style={styles.settingsItem}>
            <MaterialIcons name="assignment" size={24} color="black" />
            <Text style={styles.settingsItemTitle}>Syarat & Ketentuan</Text>
          </HorizontalView>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.logoutContainer}
        onPress={() => onLogoutClicked()}>
        <HorizontalView style={styles.logout}>
          <MaterialIcons name="exit-to-app" size={24} color="black" />
          <Text style={styles.settingsItemTitle}>Keluar</Text>
        </HorizontalView>
      </TouchableOpacity>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  profileContainer: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  profile: {
    padding: 16,
    backgroundColor: colors.normal,
    borderRadius: 12,
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 24,
    width: 48,
    height: 48,
  },
  settingContainer: {
    marginTop: 16,
    marginHorizontal: 16,
    borderRadius: 12,
    backgroundColor: colors.normal,
  },
  settingsItem: {
    padding: 16,
    alignItems: 'center',
  },
  settingsItemTitle: {
    marginStart: 16,
    flexGrow: 1,
    color: colors.BLACK,
  },
  settingsItemText: {
    color: colors.BLACK,
  },
  logoutContainer: {
    marginTop: 16,
    marginHorizontal: 16,
    borderRadius: 12,
    backgroundColor: colors.normal,
  },
  logout: {
    padding: 16,
    alignItems: 'center',
  },
});

export default AccountScreen;
