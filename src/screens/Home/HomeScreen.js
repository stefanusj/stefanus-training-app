import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  ButtonIcon,
  CardView,
  HorizontalView,
  SafeAreaContainer,
  Slider,
  TextInput,
} from '../../components';
import {colors} from '../../style/colors';
import {spacing} from '../../style/spacing';
import {types} from '../../style/types';
import BannerItem from './BannerItem';
import NewsItem from './NewsItem';

const banners = [
  {
    id: 1,
    image: require('../../assets/images/banner_1.jpg'),
  },
  {
    id: 2,
    image: require('../../assets/images/banner_2.jpg'),
  },
  {
    id: 3,
    image: require('../../assets/images/banner_3.jpg'),
  },
];

const news = [
  {
    id: 1,
    title: 'Bantu sesama',
    donated: 1000000,
    total: 2000000,
    image: require('../../assets/images/news_1.jpg'),
  },
  {
    id: 2,
    title: 'Tolong menolong',
    donated: 1000000,
    total: 7000000,
    image: require('../../assets/images/news_2.jpg'),
  },
  {
    id: 3,
    title: 'Bantu sesama',
    donated: 1000000,
    total: 4000000,
    image: require('../../assets/images/news_3.jpg'),
  },
  {
    id: 4,
    title: 'Tolong menolong',
    donated: 1000000,
    total: 2500000,
    image: require('../../assets/images/news_4.jpg'),
  },
];

const HomeScreen = ({navigation}) => {
  const onDonationClicked = () => {
    navigation.navigate('Donation');
  };
  const onStatisticsClicked = () => {
    navigation.navigate('Statistics');
  };
  const onHistoryClicked = () => {
    navigation.navigate('History');
  };
  const onDonationCreateClicked = () => {
    navigation.navigate('DonationCreate');
  };

  return (
    <SafeAreaContainer>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HorizontalView style={styles.topBar}>
          <ButtonIcon
            icon="apps"
            size={48}
            iconSize={24}
            type="outline"
            style={spacing.mh16}
          />
          <TextInput style={styles.searchBar} placeholder="Cari disini.." />
          <ButtonIcon
            icon="favorite"
            size={48}
            iconSize={24}
            type="outline"
            style={spacing.mh16}
          />
        </HorizontalView>
        <CardView style={styles.cardViewWallet}>
          <HorizontalView style={styles.balanceWallet}>
            <TouchableOpacity>
              <Text>Saldo</Text>
              <Text style={types.h6}>Rp120.000</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.walletItem}>
                <MaterialIcons name="add-box" size={24} />
                <Text>Isi</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.walletItem}>
                <MaterialIcons name="history" size={24} />
                <Text>Riwayat</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.walletItem}>
                <MaterialIcons name="widgets" size={24} />
                <Text>Lainnya</Text>
              </View>
            </TouchableOpacity>
          </HorizontalView>
        </CardView>
        <Slider
          data={banners}
          renderItem={({item}) => BannerItem(item)}
          activeDotStyle={{backgroundColor: colors.WHITE}}
          keyExtractor={(item) => item.id.toString()}
        />
        <HorizontalView style={styles.menu}>
          <TouchableOpacity onPress={() => onDonationClicked()}>
            <View style={styles.menuItem}>
              <View style={styles.menuIconContainer}>
                <MaterialIcons name="payments" color="forestgreen" size={24} />
              </View>
              <Text style={[types.subtitle2, spacing.mt8]}>Donasi</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onStatisticsClicked()}>
            <View style={styles.menuItem}>
              <View style={styles.menuIconContainer}>
                <MaterialIcons name="leaderboard" color="steelblue" size={24} />
              </View>
              <Text style={[types.subtitle2, spacing.mt8]}>Statistik</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onHistoryClicked()}>
            <View style={styles.menuItem}>
              <View style={styles.menuIconContainer}>
                <MaterialIcons name="history" color="tomato" size={24} />
              </View>
              <Text style={[types.subtitle2, spacing.mt8]}>Riwayat</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => onDonationCreateClicked()}>
            <View style={styles.menuItem}>
              <View style={styles.menuIconContainer}>
                <MaterialIcons name="support" color="violet" size={24} />
              </View>
              <Text style={[types.subtitle2, spacing.mt8]}>Bantu</Text>
            </View>
          </TouchableOpacity>
        </HorizontalView>
        <View>
          <Text style={[types.subtitle1, spacing.ms16]}>
            Penggalangan dana mendesak
          </Text>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={spacing.mv16}
            data={news}
            extraData={news}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => NewsItem(item)}
          />
        </View>
      </ScrollView>
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  topBar: {
    alignItems: 'center',
    paddingVertical: 16,
  },
  searchBar: {
    flex: 1,
  },
  cardViewWallet: {
    padding: 16,
    marginHorizontal: 16,
  },
  balanceWallet: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  walletItem: {
    alignItems: 'center',
  },
  menu: {
    padding: 16,
    justifyContent: 'space-around',
  },
  menuIconContainer: {
    padding: 12,
    borderRadius: 12,
    backgroundColor: colors.normal,
  },
  menuItem: {
    alignItems: 'center',
  },
});

export default HomeScreen;
