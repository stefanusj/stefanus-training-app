import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {CardView, CurrencyFormat, ProgressBar} from '../../components';
import {spacing} from '../../style/spacing';
import {types} from '../../style/types';

const NewsItem = (news) => {
  return (
    <TouchableOpacity>
      <CardView style={styles.container}>
        <Image source={news.image} style={styles.image} />
        <View style={styles.textContainer}>
          <Text style={[types.subtitle2]}>{news.title}</Text>
          <ProgressBar
            progress={news.donated / news.total}
            style={spacing.mv8}
          />
          <CurrencyFormat
            value={news.total}
            style={spacing.mt8}
            renderText={(formattedValue) => (
              <Text>Total: {formattedValue}</Text>
            )}
          />
        </View>
      </CardView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginStart: 16,
    width: 256,
  },
  image: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    width: 256,
    height: 144,
  },
  textContainer: {
    flex: 1,
    padding: 8,
  },
});

export default NewsItem;
