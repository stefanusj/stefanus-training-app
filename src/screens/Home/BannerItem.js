import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {Container} from '../../components';
import {spacing} from '../../style/spacing';

const BannerItem = (intro) => {
  return (
    <Container style={spacing.p16}>
      <Image source={intro.image} style={styles.image} />
    </Container>
  );
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 192,
    borderRadius: 24,
  },
});

export default BannerItem;
