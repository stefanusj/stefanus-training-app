import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import WebView from 'react-native-webview';
import {SafeAreaContainer} from '../../components';

const PaymentScreen = ({route}) => {
  const [screen, setScreen] = useState({
    midtrans: undefined,
  });

  useEffect(() => {
    setScreen({...screen, midtrans: route.params?.midtrans});
  }, []);

  return (
    <SafeAreaContainer>
      <WebView
        style={styles.webView}
        source={{uri: screen.midtrans?.redirect_url}}
      />
    </SafeAreaContainer>
  );
};

const styles = StyleSheet.create({
  webView: {
    flex: 1,
  },
});

export default PaymentScreen;
