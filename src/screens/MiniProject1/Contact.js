import React, {useContext} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {RootContext} from './index';
import {types} from '../../style/types';
import Button from '../../components/Button';
import CardView from '../../components/CardView';
import Label from '../../components/Label';
import TextInput from '../../components/TextInput';

const Contact = () => {
  const state = useContext(RootContext);

  const ContactItem = ({contact}) => (
    <View style={styles.contactContainer}>
      <View style={styles.contactContent}>
        <Text>{contact.name}</Text>
        <Text>Telepon: {contact.phone}</Text>
      </View>
      <TouchableOpacity onPress={() => state.destroyContact(contact.phone)}>
        <Ionicons name="trash-outline" size={24} />
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={styles.container}>
      <CardView style={styles.formContainer}>
        <Label>Nama kontak</Label>
        <TextInput
          placeholder="Eg. John Doe"
          value={state.name}
          onChangeText={(text) => state.setName(text)}
        />
        <Label>Nomor telepon</Label>
        <TextInput
          placeholder="Eg. 08811223344"
          keyboardType="numeric"
          value={state.phone}
          onChangeText={(text) => state.setPhone(text)}
        />
        <Button
          onPress={() => state.storeContact()}
          style={styles.button}
          horizontal={true}>
          <Ionicons name="add-outline" size={24} color="black" />
          <Text>Tambah</Text>
        </Button>
      </CardView>
      <Text style={{...types.subtitle2, ...styles.subtitle}}>
        Daftar Kontak
      </Text>
      <FlatList
        data={state.contacts}
        renderItem={(contact) => <ContactItem contact={contact.item} />}
        keyExtractor={(contact) => contact.phone}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 16,
  },
  formContainer: {
    marginTop: 8,
  },
  subtitle: {
    marginTop: 16,
    marginBottom: 8,
  },
  contactContainer: {
    flexDirection: 'row',
    borderColor: 'lightgray',
    borderWidth: 1,
    borderRadius: 8,
    marginVertical: 4,
    padding: 16,
  },
  contactContent: {
    flex: 1,
  },
});

export default Contact;
