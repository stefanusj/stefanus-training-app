import React, {createContext, useState} from 'react';
import Contact from './Contact';

export const RootContext = createContext();
const Provider = RootContext.Provider;

const MiniProject1 = () => {
  const [contacts, setContacts] = useState([]);
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');

  const storeContact = () => {
    setContacts([...contacts, {name, phone}]);
    setName('');
    setPhone('');
  };

  const destroyContact = (phone) => {
    setContacts(contacts.filter((contact) => contact.phone !== phone));
  };

  const data = {
    contacts,
    name,
    setName,
    phone,
    setPhone,
    storeContact,
    destroyContact,
  };

  return (
    <Provider value={data}>
      <Contact />
    </Provider>
  );
};
export default MiniProject1;
