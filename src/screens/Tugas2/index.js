import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tugas2 = () => {
  return (
    <>
      <View>
        <ActionBar />
        <Profile />
        <Settings />
      </View>
    </>
  );
};

const ActionBar = () => {
  return (
    <View style={styles.actionBar}>
      <Text style={styles.h6}>Account</Text>
    </View>
  );
};

const Profile = () => {
  return (
    <View style={styles.profile}>
      <Image
        source={{
          uri: 'https://stefanusj.com/storage/users/stefanus/formal.jpg',
        }}
        style={styles.iconM}
      />
      <Text style={styles.fullname}>Stefanus Julianto</Text>
    </View>
  );
};

const Settings = () => {
  return (
    <View style={styles.settings}>
      <View style={styles.dividerS} />
      <View style={styles.settingsItem}>
        <Ionicons name="wallet-outline" size={24} color="black" />
        <Text style={styles.settingsItemTitle}>Saldo</Text>
        <Text style={styles.settingsItemText}>Rp120.000.000</Text>
      </View>
      <View style={styles.dividerM} />
      <View style={styles.settingsItem}>
        <Ionicons name="settings-outline" size={24} color="black" />
        <Text style={styles.settingsItemTitle}>Pengaturan</Text>
      </View>
      <View style={styles.dividerS} />
      <View style={styles.settingsItem}>
        <Ionicons name="help-circle-outline" size={24} color="black" />
        <Text style={styles.settingsItemTitle}>Bantuan</Text>
      </View>
      <View style={styles.dividerS} />
      <View style={styles.settingsItem}>
        <Ionicons name="newspaper-outline" size={24} color="black" />
        <Text style={styles.settingsItemTitle}>Syarat & Ketentuan</Text>
      </View>
      <View style={styles.dividerM} />
      <View style={styles.settingsItem}>
        <Ionicons name="log-out-outline" size={24} color="black" />
        <Text style={styles.settingsItemTitle}>Keluar</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  actionBar: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingStart: 16,
    backgroundColor: '#0496FF',
  },
  h6: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },

  profile: {
    flexDirection: 'row',
    padding: 16,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  iconM: {
    borderRadius: 24,
    width: 48,
    height: 48,
  },
  fullname: {
    fontSize: 18,
    marginStart: 16,
    fontWeight: 'bold',
    color: 'black',
  },

  settings: {},
  settingsItem: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    color: 'black',
    backgroundColor: 'white',
  },
  settingsItemTitle: {
    marginStart: 16,
    flexGrow: 1,
    color: 'black',
  },
  settingsItemText: {
    color: 'black',
  },

  dividerS: {
    height: 1,
  },
  dividerM: {
    height: 4,
  },
});

export default Tugas2;
