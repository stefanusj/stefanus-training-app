import React, {useEffect, useState} from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AccountScreen from '../screens/Account/AccountScreen';
import AccountEditScreen from '../screens/Account/AccountEdit/AccountEditScreen';
import LoginScreen from '../screens/Auth/Login/LoginScreen';
import RegisterScreen from '../screens/Auth/Register/RegisterScreen';
import VerificationScreen from '../screens/Auth/Verification/VerificationScreen';
import PasswordScreen from '../screens/Auth/Password/PasswordScreen';
import HelpScreen from '../screens/Help/HelpScreen';
import HomeScreen from '../screens/Home/HomeScreen';
import IntroScreen from '../screens/Intro/IntroScreen';
import DonationScreen from '../screens/Donation/DonationScreen';
import DonationCreateScreen from '../screens/Donation/Create/DonationCreateScreen';
import DonationDetailScreen from '../screens/Donation/Detail/DonationDetailScreen';
import DonationDonateScreen from '../screens/Donation/Donate/DonationDonateScreen';
import HistoryScreen from '../screens/History/HistoryScreen';
import InboxScreen from '../screens/Inbox/InboxScreen';
import PaymentScreen from '../screens/Payment/PaymentScreen';
import SplashScreen from '../screens/SplashScreen/SplashScreen';
import StatisticsScreen from '../screens/Statistics/StatisticsScreen';
import {AsyncStorageToken} from '../constants';
import {colors} from '../style/colors';
import {spacing} from '../style/spacing';

const Stack = createStackNavigator();
const AccountStack = createStackNavigator();
const HomeStack = createStackNavigator();
const InboxStack = createStackNavigator();

const DashboardTab = createMaterialBottomTabNavigator();

const AccountStackScreen = () => (
  <AccountStack.Navigator
    screenOptions={{
      headerTitleAlign: 'center',
      headerStyle: {
        elevation: 0,
      },
    }}>
    <AccountStack.Screen
      name="Account"
      component={AccountScreen}
      options={{headerTitle: 'Akun Saya'}}
    />
  </AccountStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerTitleAlign: 'center',
      headerStyle: {
        elevation: 0,
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{headerShown: false}}
    />
  </HomeStack.Navigator>
);

const InboxStackScreen = () => (
  <InboxStack.Navigator
    screenOptions={{
      headerTitleAlign: 'center',
      headerStyle: {
        elevation: 0,
      },
    }}>
    <AccountStack.Screen
      name="Inbox"
      component={InboxScreen}
      options={{headerTitle: 'Inbox'}}
    />
  </InboxStack.Navigator>
);

const DashboardStackScreen = () => (
  <DashboardTab.Navigator
    activeColor={colors.primaryColor}
    inactiveColor={colors.inactive}
    barStyle={[{backgroundColor: colors.WHITE}, spacing.p8]}>
    <DashboardTab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarIcon: ({color}) => (
          <MaterialIcons name="home" color={color} size={24} />
        ),
      }}
    />
    <DashboardTab.Screen
      name="Inbox"
      component={InboxStackScreen}
      options={{
        tabBarBadge: true,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="chat-bubble" color={color} size={24} />
        ),
      }}
    />
    <DashboardTab.Screen
      name="Account"
      component={AccountStackScreen}
      options={{
        tabBarIcon: ({color}) => (
          <MaterialIcons name="account-circle" color={color} size={24} />
        ),
      }}
    />
  </DashboardTab.Navigator>
);

const RootNavigation = ({isLoggedIn}) => (
  <Stack.Navigator
    initialRouteName={isLoggedIn ? 'Dashboard' : 'Intro'}
    screenOptions={{
      headerTitleAlign: 'center',
      headerStyle: {
        elevation: 0,
      },
    }}>
    <Stack.Screen
      name="Intro"
      component={IntroScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{headerTitle: ''}}
    />
    <Stack.Screen
      name="Register"
      component={RegisterScreen}
      options={{headerTitle: ''}}
    />
    <Stack.Screen
      name="Verification"
      component={VerificationScreen}
      options={{headerTitle: ''}}
    />
    <Stack.Screen
      name="Password"
      component={PasswordScreen}
      options={{headerTitle: ''}}
    />
    <Stack.Screen
      name="Dashboard"
      component={DashboardStackScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Donation"
      component={DonationScreen}
      options={{headerTitle: 'Donasi'}}
    />
    <Stack.Screen
      name="DonationCreate"
      component={DonationCreateScreen}
      options={{headerTitle: 'Buat Donasi'}}
    />
    <Stack.Screen
      name="DonationDetail"
      component={DonationDetailScreen}
      options={{headerTitle: 'Donasi'}}
    />
    <Stack.Screen
      name="DonationDonate"
      component={DonationDonateScreen}
      options={{headerTitle: 'Donasi'}}
    />
    <Stack.Screen
      name="Payment"
      component={PaymentScreen}
      options={{headerTitle: 'Pembayaran'}}
    />
    <Stack.Screen
      name="AccountEdit"
      component={AccountEditScreen}
      options={{headerTitle: 'Ubah Akun'}}
    />
    <Stack.Screen
      name="Statistics"
      component={StatisticsScreen}
      options={{headerTitle: 'Statistik'}}
    />
    <Stack.Screen
      name="History"
      component={HistoryScreen}
      options={{headerTitle: 'Riwayat'}}
    />
    <Stack.Screen
      name="Help"
      component={HelpScreen}
      options={{headerTitle: 'Bantuan'}}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const checkIfLoggedIn = async () => {
    try {
      const token = await AsyncStorage.getItem(AsyncStorageToken);
      setIsLoggedIn(token != null);
    } catch (exception) {
      console.log(exception);
    }
  };

  useEffect(() => {
    checkIfLoggedIn();

    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <RootNavigation isLoggedIn={isLoggedIn} />
    </NavigationContainer>
  );
};

export default AppNavigation;
