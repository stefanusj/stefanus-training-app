import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {types} from '../style/types';

const Label = ({style, ...props}) => {
  return (
    <Text style={[types.subtitle2, styles.label, style]} {...props}>
      {props.children}
    </Text>
  );
};

const styles = StyleSheet.create({
  label: {
    marginBottom: 4,
  },
});

export default Label;
