import React from 'react';
import NumberFormat from 'react-number-format';

const CurrencyFormat = ({value, style, width, ...props}) => {
  return <NumberFormat value={value} {...props} />;
};

CurrencyFormat.defaultProps = {
  decimalSeparator: ',',
  displayType: 'text',
  prefix: 'Rp',
  thousandSeparator: '.',
};

export default CurrencyFormat;
