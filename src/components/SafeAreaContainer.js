import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {colors} from '../style/colors';

const SafeAreaContainer = ({style, ...props}) => {
  return (
    <SafeAreaView style={[styles.container, style]} {...props}>
      {props.children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
});

export default SafeAreaContainer;
