import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../style/colors';

const CardView = ({style, ...props}) => {
  return (
    <View style={[styles.cardView, style]} {...props}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  cardView: {
    borderRadius: 16,
    backgroundColor: colors.normal,
  },
});

export default CardView;
