import React from 'react';
import {StyleSheet, Text, TouchableHighlight} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../style/colors';
import {types} from '../style/types';

const Button = ({style, text, type, ...props}) => {
  return (
    <TouchableHighlight
      underlayColor={colors.divider}
      style={[
        styles.button,
        type === 'solid' ? styles.solid : '',
        type === 'light' ? styles.light : '',
        type === 'outline' ? styles.outline : '',
        style,
      ]}
      {...props}>
      <Text
        style={[
          types.button,
          type === 'solid' ? {color: colors.onPrimaryColor} : '',
          type === 'light' ? {color: colors.primaryColor} : '',
          type === 'outline' ? {color: colors.BLACK} : '',
        ]}>
        {text}
      </Text>
    </TouchableHighlight>
  );
};

export const ButtonIcon = ({icon, size, iconSize, type, style, ...props}) => {
  return (
    <TouchableHighlight
      underlayColor={colors.divider}
      style={[
        styles.buttonIcon(size),
        type === 'solid' ? styles.solid : '',
        type === 'light' ? styles.light : '',
        type === 'outline' ? styles.outline : '',
        style,
      ]}
      {...props}>
      <MaterialIcons
        name={icon}
        size={iconSize}
        color={
          type === 'solid'
            ? colors.onPrimaryColor
            : type === 'light'
            ? colors.primaryColor
            : type === 'outline'
            ? colors.BLACK
            : colors.focused
        }
      />
    </TouchableHighlight>
  );
};

Button.defaultProps = {
  title: '',
  onPress: () => console.log('Please attach a method to this component'),
};

ButtonIcon.defaultProps = {
  size: 36,
  iconSize: 24,
  onPress: () => console.log('Please attach a method to this component'),
};

const styles = StyleSheet.create({
  button: {
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 48,
    backgroundColor: colors.normal,
  },
  buttonIcon: (size) => ({
    ...styles.button,
    width: size,
    height: size,
    borderRadius: size,
  }),
  solid: {
    backgroundColor: colors.primaryColor,
  },
  light: {
    backgroundColor: colors.WHITE,
  },
  outline: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: colors.normal,
  },
});

export default Button;
