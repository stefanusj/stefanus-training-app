import React from 'react';
import {TextInput as BaseTextInput, StyleSheet} from 'react-native';
import {TextInputMask as BaseTextInputMask} from 'react-native-masked-text';
import {colors} from '../style/colors';
import {ButtonIcon} from './Button';
import HorizontalView from './HorizontalView';

const TextInput = ({multiline, style, ...props}) => {
  return (
    <BaseTextInput
      style={[
        styles.textInput,
        multiline ? {textAlignVertical: 'top'} : '',
        style,
      ]}
      multiline={multiline}
      {...props}
    />
  );
};

export const TextInputMask = ({style, ...props}) => {
  return <BaseTextInputMask style={[styles.textInput, style]} {...props} />;
};

export const TextInputEndIcon = ({
  icon,
  style,
  textInputIconStyle,
  onIconCLicked,
  ...props
}) => {
  return (
    <HorizontalView>
      <TextInput style={[styles.textInputEndIcon, style]} {...props} />
      <ButtonIcon
        icon={icon}
        style={[styles.textInputEndIconButton, textInputIconStyle]}
        onPress={() => onIconCLicked()}
      />
    </HorizontalView>
  );
};

const styles = StyleSheet.create({
  textInput: {
    borderRadius: 12,
    backgroundColor: colors.normal,
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
  textInputEndIcon: {
    flexGrow: 1,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  textInputEndIconButton: {
    height: 'auto',
    width: 48,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 8,
  },
});

export default TextInput;
