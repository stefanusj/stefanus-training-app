import React from 'react';
import {StyleSheet, View} from 'react-native';

const Container = ({style, ...props}) => {
  return (
    <View style={[styles.container, style]} {...props}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Container;
