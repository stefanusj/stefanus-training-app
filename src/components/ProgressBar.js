import React from 'react';
import * as Progress from 'react-native-progress';
import {colors} from '../style/colors';

const ProgressBar = ({progress, style, width, ...props}) => {
  return (
    <Progress.Bar
      progress={progress}
      style={style}
      width={width}
      color={colors.primaryColor}
      unfilledColor={colors.inactive}
      borderWidth={0}
      {...props}
    />
  );
};

ProgressBar.defaultProps = {
  width: null,
};

export default ProgressBar;
