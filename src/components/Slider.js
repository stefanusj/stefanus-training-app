import React from 'react';
import AppIntroSlider from 'react-native-app-intro-slider';
import {colors} from '../style/colors';

const Slider = ({style, ...props}) => {
  return (
    <AppIntroSlider
      style={style}
      activeDotStyle={{backgroundColor: colors.primaryColor}}
      showPrevButton={false}
      showNextButton={false}
      showDoneButton={false}
      {...props}
    />
  );
};

export default Slider;
