import React from 'react';
import {View} from 'react-native';
import {colors} from '../style/colors';

const Divider = ({height, backgroundColor, style, ...props}) => {
  return <View style={[{height, backgroundColor}, style]} {...props} />;
};

Divider.defaultProps = {
  height: 1,
  backgroundColor: colors.divider,
};

export default Divider;
