import React from 'react';
import {StyleSheet, View} from 'react-native';

const HorizontalView = ({style, ...props}) => {
  return (
    <View style={[styles.horizontalView, style]} {...props}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  horizontalView: {
    flexDirection: 'row',
  },
});

export default HorizontalView;
