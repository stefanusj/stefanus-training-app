import React from 'react';
import {StyleSheet} from 'react-native';
import BaseOTPInputView from '@twotalltotems/react-native-otp-input/dist';
import {colors} from '../style/colors';

const OTPInputView = ({style, ...props}) => {
  return (
    <BaseOTPInputView
      style={[styles.otpInput, style]}
      codeInputFieldStyle={styles.textInput}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  textInput: {
    color: colors.BLACK,
    borderRadius: 12,
    borderWidth: 0,
    backgroundColor: colors.normal,
  },
  otpInput: {
    height: 150,
  },
});

export default OTPInputView;
