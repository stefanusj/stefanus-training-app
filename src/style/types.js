import {StyleSheet} from 'react-native';
import {colors} from './colors';

export const types = StyleSheet.create({
  h4: {
    fontSize: 34,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  h5: {
    fontSize: 24,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  h6: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  subtitle1: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  subtitle2: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  body1: {
    fontSize: 16,
    color: colors.BLACK,
  },
  body2: {
    fontSize: 14,
    color: colors.BLACK,
  },
  button: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.BLACK,
  },
  normal: {
    fontSize: 12,
    color: colors.focused,
  },
});
