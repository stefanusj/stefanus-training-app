import {StyleSheet} from 'react-native';

export const spacing = StyleSheet.create({
  m8: {
    margin: 8,
  },
  mt8: {
    marginTop: 8,
  },
  mt16: {
    marginTop: 16,
  },
  mt32: {
    marginTop: 32,
  },
  mt64: {
    marginTop: 64,
  },
  mb8: {
    marginBottom: 8,
  },
  ms16: {
    marginStart: 16,
  },
  mh16: {
    marginHorizontal: 16,
  },
  mv8: {
    marginVertical: 8,
  },
  mv16: {
    marginVertical: 16,
  },
  p8: {
    padding: 8,
  },
  p16: {
    padding: 16,
  },
  ph16: {
    paddingHorizontal: 16,
  },
});
