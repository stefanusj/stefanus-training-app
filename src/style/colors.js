export const colors = {
  primaryColor: '#0496FF',
  onPrimaryColor: '#FFFFFF',
  divider: '#DADCE0',

  normal: '#F1F3FC',
  inactive: '#DADCE0',
  focused: '#888888',

  BLACK: '#201211',
  WHITE: '#FFFFFF',
};
