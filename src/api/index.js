const BASE_URL = 'https://crowdfunding.sanberdev.com';
const API_URL = `${BASE_URL}/api`;

export const LOGIN = `${API_URL}/auth/login`;
export const REGISTER = `${API_URL}/auth/register`;
export const REGENERATE_OTP = `${API_URL}/auth/regenerate-otp`;
export const VERIFICATION = `${API_URL}/auth/verification`;
export const UPDATE_PASSWORD = `${API_URL}/auth/update-password`;

export const GET_DONATION = `${API_URL}/donasi/daftar-donasi`;
export const PAY_DONATION = `${API_URL}/donasi/generate-midtrans`;
export const GET_HISTORY = `${API_URL}/donasi/riwayat-transaksi`;
export const STORE_DONATION = `${API_URL}/donasi/tambah-donasi`;

export const GET_PROFILE = `${API_URL}/profile/get-profile`;
export const UPDATE_PROFILE = `${API_URL}/profile/update-profile`;

export const imageUri = (uri) => `${BASE_URL}${uri}`;

export default BASE_URL;
