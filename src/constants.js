export const webClientId =
  '302493114200-olum10c3rssealrmchqhhbam0tnbbrj3.apps.googleusercontent.com';

export const mapBoxAccessToken =
  'sk.eyJ1Ijoic3RlZmFudXNqIiwiYSI6ImNra2Vnc3I2NjBqZmsyeHF0dmtyejM2cXcifQ.IHh9k71NtfBb4qthcKrZbA';

// AsyncStorage Item Key
export const AsyncStorageToken = 'token';
